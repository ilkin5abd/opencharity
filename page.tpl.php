<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<div id="header">
    <div class="container">
        <div class="row align-x-justify align-y-middle nowrap">
            <div class="column shrink">
				<?php if ( $logo ): ?>
                    <a href="<?php print $front_page; ?>" title="<?php print t( 'Home' ); ?>" rel="home" id="logo">
                        <img src="<?php print $logo; ?>" alt="<?php print t( 'Home' ); ?>"/>
                    </a>
				<?php endif; ?>
            </div>
            <div class="column shrink">
				<?php if ( $main_menu ): ?>
					<?php print theme( 'links__system_main_menu', array(
						'links'      => $main_menu,
						'attributes' => array(
							'id'    => 'main-menu',
							'class' => array(
								'links',
								'inline',
								'clearfix'
							)
						),
						'heading'    => null
					) ); ?>
				<?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php if ( $hero ): ?>
    <div id="hero" class="hero" <?php if ( ! empty( $hero['image'] ) )
		print ' style="background-image:url(' . $hero['image'] . ');"' ?>>
        <p class="hero__title"><?php print $hero['title'] ?></p>
        <div class="container">
            <div class="row align-x-center">
                <div class="column column-6 small-12">
					<?php print $hero['desc']; ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ( $page['next_event'] ): ?>
    <div id="next-event">
        <div class="container">
            <div class="row">
                <div class="column column-12">
					<?php print render( $page['next_event'] ) ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<div id="get-involved">
	<?php if ( $page['gi_first'] || $page['gi_second'] || $page['gi_last'] ): ?>
        <h2>Get involved</h2>
	<?php endif; ?>
	<?php if ( $page['gi_first'] || $page['gi_first'] || $page['gi_first'] ): ?>
        <div class="container">
            <div class="row">
				<?php if ( $page['gi_first'] ): ?>
                    <div class="column column-4 medium-12 small-12">
						<?php print render( $page['gi_first'] ) ?>
                    </div>
				<?php endif; ?>
				<?php if ( $page['gi_second'] ): ?>
                    <div class="column column-4 medium-12 small-12">
						<?php print render( $page['gi_second'] ) ?>
                    </div>
				<?php endif; ?>
				<?php if ( $page['gi_last'] ): ?>
                    <div class="column column-4 medium-12 small-12">
						<?php print render( $page['gi_last'] ) ?>
                    </div>
				<?php endif; ?>
            </div>
        </div>
	<?php endif; ?>
</div>

<div id="our-mission" class="om">
	<?php if ( $page['om_first'] || $page['om_second'] || $page['om_last'] || $page['om_top'] ) : ?>
        <h2>Our mission</h2>
	<?php endif; ?>
	<?php if ( $page['om_first'] || $page['om_second'] || $page['om_last'] || $page['om_top'] ) : ?>
        <div class="container">
			<?php if ( $page['om_top'] ): ?>
                <div class="row">
                    <div class="column column-12"><?php print render( $page['om_top'] ); ?></div>
                </div>
			<?php endif; ?>
			<?php if ( $page['om_first'] || $page['om_second'] || $page['om_last'] ) : ?>
                <div class="row">
					<?php if ( $page['om_first'] ): ?>
                        <div class="column column-4 medium-12 small-12">
							<?php print render( $page['om_first'] ) ?>
                        </div>
					<?php endif; ?>
					<?php if ( $page['om_second'] ): ?>
                        <div class="column column-4 medium-12 small-12">
							<?php print render( $page['om_second'] ) ?>
                        </div>
					<?php endif; ?>
					<?php if ( $page['om_last'] ): ?>
                        <div class="column column-4 medium-12 small-12">
							<?php print render( $page['om_last'] ) ?>
                        </div>
					<?php endif; ?>
                </div>
                <div class="row">
                    <div class="column column-12">
                        <div class="om__border"></div>
                    </div>
                </div>
			<?php endif; ?>
        </div>
	<?php endif; ?>
</div>

<?php if ( $page['members'] ): ?>
    <div id="members" class="members">
        <h2>Our members</h2>
        <div class="container">
            <div class="row">
                <div class="column column-12">
					<?php print render( $page['members'] ); ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ( $page['blog'] ): ?>
    <div id="blog">
        <h2>Blog</h2>
        <?php print render( $page['blog'] ) ?>
    </div>
<?php endif; ?>

<div class="container">
    <div class="row">
        <div class="column column-12">
		    <?php print $messages; ?>
		    <?php if ( $tabs ): ?>
                <div class="tabs"><?php print render( $tabs ); ?></div>
		    <?php endif; ?>
		    <?php print render( $page['content'] ); ?>
        </div>
    </div>
</div>

<?php if ( $page['footer'] ): ?>
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="column column-12">
	                <?php print render( $page['footer'] ) ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php /*print render( $page['help'] ); */?>
<?php /*print $feed_icons; */?>
<?php /*print render( $title_prefix ); */?>
<?php /*if ( $title ): */ ?><!--
    <h1 class="title" id="page-title"><?php /*print $title; */ ?></h1>
--><?php /*endif; */ ?>
<?php /*print render( $title_suffix ); */?>
<?php /*if ( $action_links ): */?><!--
    <ul class="action-links"><?php /*print render( $action_links ); */?></ul>
--><?php /*endif; */?>
<?php /*if ( $page['highlighted'] ): */ ?><!--
    <div id="highlighted"><?php /*print render( $page['highlighted'] ); */ ?></div>
--><?php /*endif; */ ?>
