(function ($, Drupal, window, document, undefined) {
  'use strict';

  Drupal.behaviors.respinsiveMenu = {
    attach: function (context, settings) {
      var header = document.getElementById("header");
      var sticky = header.offsetTop;

      $(document.body, context).once(function () {
        window.onscroll = function () {
          stickyListener();
        }
      });

      function stickyListener() {
        if (window.pageYOffset > sticky) {
          header.classList.add('sticky');
        }
        else {
          header.classList.remove('sticky');
        }
      }
    }
  };

}(jQuery, Drupal, this, this.document));