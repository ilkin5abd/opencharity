## My soulition

I choosed this approach because:

1. Drupal Official site recommends it
2. This approach is simple and clear

### 1. Created .info file

1. Defined theme name,description,core
2. Defined theme regions
3. Defined paths to css,js

### 2. Copied images,fonts to theme root

### 3. Created main theme files

1. Copied page.tpl.php to theme root
2. Created template.php to manipulate page variables
3. Created theme-settings.php to add form elements to theme 
   settings section in Appearance
4. Created app.js file to add some drupal behaviors
   
### 4. Setted up gulp task runner

1. Instaled gulp with npm
2. Installed modules for scss compiling
3. Installed foundation with bower ONLY FOR TO USE 
   MIXINS AND NORMALIZE styles =).Didnt use any library classes 

### 5. Used blocks and views

1. Used Blocks for Get Involved,Our Mission,Footer regions (static content)
2. Used Views for Our Members,Blog,Next Event regions (dynamic content)

### 6. Installed some modules

1. Installed responsive-menu module,overrided only css in my style.css
2. Installed Devel module to debug page variables via magic function dpm()
3. Installed Slick slider module for rendering slider content via Views
