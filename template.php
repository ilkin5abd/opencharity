<?php

function oc_preprocess_html ( &$variables ) {
	$element = array(
		'#type'       => 'html_tag',
		'#tag'        => 'meta',
		'#attributes' => array( 'http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge,chrome=1' ),
	);
	drupal_add_html_head( $element, 'http_equiv' );
	$element = array(
		'#type'       => 'html_tag',
		'#tag'        => 'meta',
		'#attributes' => array(
			'name'    => 'viewport',
			'content' => 'width=device-width, user-scalable=no, initial-scale=1.0'
		),
	);
	drupal_add_html_head( $element, 'viewport' );
}

function oc_preprocess_page ( &$variables ) {
	$variables['hero'] = null;
	if ( drupal_is_front_page() ) {
		$fid        = theme_get_setting( 'hero_image' );
		$image      = ! empty( $fid ) ? file_load( $fid ) : null;
		$hero_image = ! empty( $image ) ? file_create_url( $image->uri ) : null;

		$hero_desc         = ( $desc = theme_get_setting( 'hero_desc' ) ) ? $desc['value'] : null;
		$hero_title        = theme_get_setting( 'hero_title' );
		$variables['hero'] = array(
			'title' => $hero_title,
			'desc'  => $hero_desc,
			'image' => $hero_image
		);
	}
}