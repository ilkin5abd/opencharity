var gulp = require("gulp"),
    sass = require("gulp-sass"),
    autoprefixer = require("gulp-autoprefixer"),
    plumber = require("gulp-plumber"),
    sourcemaps = require('gulp-sourcemaps');

var paths =
    {
      scss:
          {
            lib: [
              __dirname + "/bower_components/foundation-sites/scss"
            ],
            main: "scss/style.scss",
            watch: "scss/**/*.scss"
          }
    };

gulp.task("scss", function () {
  return gulp.src(paths.scss.main)
      // .pipe(sourcemaps.init())
      .pipe(plumber())
      .pipe(sass({outputStyle: 'compressed'/*, includePaths: paths.scss.lib*/}))
      // .pipe(sourcemaps.write())
      .pipe(autoprefixer(['last 2 versions', '> 1%', 'ie 8', 'ie 7', 'iOS >= 8', 'Safari >= 8']))
      .pipe(gulp.dest('css'))
});

gulp.task("watch", ["scss"], function () {
  gulp.watch(paths.scss.watch, ["scss"]);
});