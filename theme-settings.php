<?php

function oc_form_system_theme_settings_alter ( &$form, &$form_state ) {

	// Load the file via file.fid.
	if ( isset( $form_state['values'] ) && isset( $form_state['values']['hero_image'] ) ) {
		$file  = file_load( $form_state['values']['hero_image'] );
		$theme = $form_state['build_info']['args'][0];
		// Change status to permanent.
		$file->status = FILE_STATUS_PERMANENT;
		file_save( $file );
		file_usage_add($file, $theme, 'theme', 1);
		dpm( 'file status changed to permanent' );
	}

	$form['hero']               = array(
		'#type'        => 'fieldset',
		'#title'       => t( 'Hero' ),
		'#description' => t( 'Customize hero region' ),
	);
	$form['hero']['hero_title'] = array(
		'#type'          => 'textfield',
		'#title'         => t( 'Hero title' ),
		'#description'   => t( 'Set the hero text' ),
		'#default_value' => theme_get_setting( 'hero_title' )
	);
	$form['hero']['hero_desc']  = array(
		'#type'          => 'text_format',
		'#title'         => t( 'Hero description' ),
		'#description'   => t( 'Set hero description markup' ),
		'#format'        => ( $desc = theme_get_setting( 'hero_desc' ) ) ? $desc['format'] : null,
		'#default_value' => ( $desc = theme_get_setting( 'hero_desc' ) ) ? $desc['value'] : null
	);

	$form['hero']['hero_image'] = array(
		'#type'              => 'managed_file',
		'#title'             => t( 'Hero background' ),
		'#required'          => false,
		'#upload_location'   => file_default_scheme() . '://theme/',
		'#default_value'     => theme_get_setting( 'hero_image' ),
		'#upload_validators' => array(
			'file_validate_extensions' => array( 'gif png jpg jpeg' ),
		),
	);
}